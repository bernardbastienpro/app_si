import React, { Component } from 'react'
import './App.css'
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import ReactCodeInput from 'react-code-input/dist/ReactCodeInput'
import Accueil from './Components/Accueil'
import Menu from './Components/Menu'
import Navigation from './Components/Navigation'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import ReservationTable from './Components/reservationTable'
import RecapOrder from './Components/RecapOrder'
import ViewOrder from "./Components/ViewOrder";

function App() {
    return (
        <div>
            <Router>
                <Navigation />
                <Route path="/" exact component={Accueil} />
                <Route path="/Menu" exact component={Menu} />
                <Route path="/Reservation" exact component={ReservationTable} />
                <Route path="/Recap" exact component={RecapOrder} />
                <Route path="/Order" exact component={ViewOrder} />
            </Router>
        </div>
    )
}

function askCode() {
    return <ReactCodeInput type="number" fields={6} />
}

export default App
