import axios from 'axios'
import * as https from 'https'

class Service {
    constructor() {
        this.URL = 'http://185.216.27.63:3001/'
        this.agent = new https.Agent({rejectUnauthorized: false})
        let service = axios.create({
            httpsAgent: this.agent,
        })
        service.interceptors.response.use(this.handleSuccess, this.handleError)
        this.service = service
    }

    handleSuccess(response) {
        return response
    }

    handleError = (error) => {
        console.error(error.response)
        switch (error.response.status) {
            case 401:
                this.redirectTo(document, '/')
                break;
            case 404:
                this.redirectTo(document, '/404')
                break;
            default:
                this.redirectTo(document, '/500')
                break;
        }
        return Promise.reject(error)
    }

    redirectTo = (document, path) => {
        document.location = path
    }

    get(path, callback) {
        return this.service
            .get(this.URL + path, {httpsAgent: this.agent})
            .then((response) => callback(response.status, response.data))
            .catch((e) => {
                this.handleError(e)
            })
    }
    patch(path, payload, callback) {
        return this.service
            .request({
                method: 'PATCH',
                url: path,
                responseType: 'json',
                data: payload,
            })
            .then((response) => callback(response.status, response.data))
    }

    post(path, payload, callback) {
        return this.service
            .request({
                method: 'POST',
                url: this.URL + path,
                responseType: 'json',
                data: payload,
            })
            .then((response) => {
                callback(response.status, response.data)
            })
            .catch((e) => this.handleError(e))
    }

    put(path, payload, callback) {
        return this.service
            .request({
                method: 'PUT',
                url: this.URL + path,
                responseType: 'json',
                data: payload,
            })
            .then((response) => {
                callback(response.status, response.data)
            })
            .catch((e) => this.handleError(e))
    }
}

export default new Service()
