import queryString from 'query-string'
import * as React from 'react'
import Service from './API'
import Cookies from 'universal-cookie'
import Table from "react-bootstrap/Table";

export default class ViewOrder extends React.Component {
    state = {
        dataOrder: [],
        restaurant_id: undefined
    }

    constructor(props) {
        super(props)
        this.params = queryString.parse(props.location.search)
        this.cookies = new Cookies()
        //console.log(cookies.get('myCat'))
    }

    componentDidMount() {
        this.getOrders()
    }


    getOrders() {
        Service.get("admin/order/" + this.params.restaurant_id, function (s, res) {
            console.log(res)
            this.setState({ dataOrder: res.message })
            console.log(this.state.dataOrder)
        }
            .bind(this))
    }



    render() {
        return this.state.dataOrder.map((order) => {
            return (
                <div className="container-fluid m-5">
                    <h1 className="display-5 m-4">{order.username}</h1>
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Nom du produit</th>
                                <th>Quantite</th>
                            </tr>
                        </thead>
                        <tbody>

                            {
                                order.products.map((product) => {
                                    return (
                                        <tr>
                                            <td> {product.product.title} </td>
                                            <td> {product.quantity} </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody></Table>
                </div>
            )
        })
    }

}
