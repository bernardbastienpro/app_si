import React from 'react'
import {Link} from 'react-router-dom';
import Dropdown from './Dropdown';

function Navigation() {

    return (
        <nav class="navbar fixed-bottom navbar-light bg-light">
            <Link to="/">
            </Link>
            <Dropdown/>
        </nav>
    )

}

export default Navigation;