import React, {Component} from "react";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import IconButton from '@material-ui/core/IconButton';
import RemoveCircleRoundedIcon from '@material-ui/icons/RemoveCircleRounded';
import {green} from '@material-ui/core/colors';

export default class CounterButton extends Component {
    static defaultProps = {max: 5}

    constructor(props) {
        super(props);
        this.product = props.product
        this.max = this.props.max
        this.plus = this.plus.bind(this)
        this.minus = this.minus.bind(this)

        this.state = {
            value: this.product.quantity
        }

    }

    componentDidMount() {
    }

    render() {
        return (/*<div id="input_div">
            <Button value="-" id="moins" onClick={this.plus}/>
            <Button type="text" size="3" value={this.state.value} id="count"/>
            <Button type="button" value="+" id="plus" onClick={this.plus}/>
             </div> */
            <InputGroup>
                <IconButton> <RemoveCircleRoundedIcon color="secondary" fontSize="large" onClick={this.minus}/>
                </IconButton>
                <FormControl
                    readonly="readonly"
                    placeholder={this.state.value}
                    aria-label={this.state.value}
                    aria-describedby="buttonAddProduct"
                />
                <IconButton> <AddCircleRoundedIcon style={{color: green[500]}} fontSize="large" onClick={this.plus}/>
                </IconButton>
            </InputGroup>
        )
    }

    minus() {
        if (this.state.value > 0) {
            this.setState({value: this.state.value - 1})
            this.product.quantity = this.state.value - 1

            if (this.product.quantity > 0) {
                localStorage.setItem(this.product._id, JSON.stringify(this.product))
            } else {
                localStorage.removeItem(this.product._id)
            }
        }
    }

    plus() {
        if (this.state.value < this.max) {
            this.setState({value: this.state.value + 1})
            this.product.quantity = this.state.value + 1

            if (this.product.quantity > 0) {
                localStorage.setItem(this.product._id, JSON.stringify(this.product))
            } else {
                localStorage.removeItem(this.product._id)
            }
        }
    }

}
