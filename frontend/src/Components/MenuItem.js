import React, {Component} from 'react';
import {CardText} from 'reactstrap';

export default class MenuItem extends Component {

    static defaultProps = {
        title: "Produit",
        description: "",
        link: this.title,
        image_path: "https://img.icons8.com/carbon-copy/100/000000/no-image.png"
    }

    constructor(props) {
        super(props);
        this.title = props.title;
        this.link = props.link;
    }

    render() {


        return (
            <div>
                <CardText><h1>{this.title}</h1>
                </CardText>

            </div>

        );
    }
}
