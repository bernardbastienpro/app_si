import queryString from 'query-string'
import * as React from 'react'
import Service from './API'
import Cookies from 'universal-cookie'
import {Redirect} from 'react-router-dom'

export default class ReservationTable extends React.Component {
    state = {
        redirect: false,
        master: false,
        dispo: false,
        errorvalid: '',
        joined: false,
        username: '',
        restaurantID: undefined,
    }

    constructor(props) {
        super(props)
        this.params = queryString.parse(props.location.search)
        this.cookies = new Cookies()
        //console.log(cookies.get('myCat'))
    }

    componentDidMount() {
        this.getDispo()
    }

    getDispo() {
        Service.get(
            'table/disponibility/' + this.params.idTable,
            function (s, res) {
                this.setState({dispo: res.message.available})
            }.bind(this)
        )
    }

    async joinTableRequest(username) {
        Service.post(
            'table/join/' + this.params.idTable,
            {username: username},
            function (s, res) {
                if (res.error) {
                    alert('Something wrong, ' + res.message)
                } else {
                    this.setState({
                        master: res.message.customer.master,
                        joined: res.message.joined,
                        restaurantID: res.message.restaurant,
                    })
                    if (res.message.joined) {

                        this.cookies.set('restaurant', this.state.restaurantID, {
                            path: '/',
                        })
                        this.cookies.set('username', this.state.username, {
                            path: '/',
                        })
                        this.cookies.set('table_id', this.params.idTable, {
                            path: '/',
                        })
                        alert(
                            'Bienvenue ' +
                            this.state.username +
                            ', vous avez rejoint la table'
                        )
                        this.setState({redirect: true})
                    }
                }
            }.bind(this)
        )
    }

    joinTable = async (event) => {
        if ((this.state.username?.length ?? 0) < 2) {
            this.setState({
                errorvalid: 'Le pseudo doit contenir 2 caractères minimum',
            })
        } else {
            await this.joinTableRequest(this.state.username)
        }
    }

    setUsername = (event) => {
        event.preventDefault()
        this.setState({username: event.target.value})
    }

    render() {
        {
            if (this.state.redirect) return <Redirect push to="/Menu"/>
        }
        if (this.cookies?.get('username')) {
            return <Redirect to={"/Menu"}/>
        } else if (!this.state.dispo) {
            return (
                <div className="container container col-6 col-lg-6 col-sm-12 text-center mt-4">
                    <div className="alert alert-danger">
                        <h1>Cette table n'est pas disponible :(</h1>
                        <p>
                            Cette table est peut-être soit fermé soit a atteint
                            sa capacité max
                        </p>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="container container col-6 col-lg-6 col-sm-12 mt-4">
                    <h1>Cette table est disponible ! </h1>
                    <form className="form form-container">
                        <div className="form-group">
                            <label>Pseudo</label>
                            <input
                                className="form-control"
                                type="text"
                                required
                                onChange={this.setUsername}
                            />
                        </div>
                        <input
                            className="btn btn-primary"
                            type="button"
                            value="Rejoindre"
                            onClick={this.joinTable}
                        />
                    </form>
                    <div>{this.state.errorvalid}</div>
                </div>
            )
        }
    }
}
