import React from 'react';
import {Cell, Grid} from 'react-mdl';

function Accueil() {
    return (
        <div style={{width: '100%', margin: 'auto'}}>
            <h1>Vous etes dans l'accueil</h1>
            <Grid className="landing-grid">
                <Cell col={12}>
                    <img
                        src="https://cdn.iconscout.com/icon/free/png-512/avatar-367-456319.png"
                        alt="avatar"
                        className="avatar-img"
                    />

                    <div className="banner-text">
                        <h1>
                            Menu Etudiant
                        </h1>
                    </div>
                </Cell>
            </Grid>
        </div>

    )
}

export default Accueil;