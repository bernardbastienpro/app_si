import React, {useState} from 'react';
import {Dropdown, DropdownMenu, DropdownToggle} from 'reactstrap';
import CounterButton from "./CounterButton";
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import IconButton from '@material-ui/core/IconButton';
import Carte from "./Menu";
import {Button} from "react-bootstrap";

const Drop = (props) => {


    const [dropdownOpen, setDropdownOpen] = useState(false);

    const toggle = () => setDropdownOpen(prevState => !prevState);

    const shoppingList = () => Object.values(localStorage).map(data => {
        return (<div>
                {JSON.parse(data).title} <CounterButton product={JSON.parse(data)} max={5}/>
            </div>
        )

    });

    return (
        <div>
            <Dropdown isOpen={dropdownOpen} size="sm" toggle={toggle}>
                <DropdownToggle>
                    <IconButton><ShoppingBasketIcon fontSize="large"/></IconButton>
                </DropdownToggle>

                <DropdownMenu>
                    {shoppingList()
                        /*<IconButton style={{float : "right"}} >Valider<SendSharpIcon /></IconButton>*/
                    }
                </DropdownMenu>

            </Dropdown>
            <Button onClick={Carte.sendOrder}>Valider </Button>
            <a className="btn btn-success" href="/Recap" role="button">Recap</a>

        </div>
    );
}


export default Drop;
