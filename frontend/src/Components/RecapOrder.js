import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'
import Cookies from 'universal-cookie'
import Service from './API'
import { Redirect } from 'react-router-dom'

export default class RecapOrder extends Component {
    constructor(props) {
        super(props)
        localStorage.clear()
        this.cookies = new Cookies()
        this.table_id = this.cookies.get('table_id')
        this.username = this.cookies.get('username')
    }

    state = {
        redirect: false,
        countOrder: 0,
        dataOrder: [],
        errorCategory: false,
    }

    componentDidMount() {
        this.initDataApi()
    }

    initDataApi() {
        this.getOrders()
    }

    getOrders() {
        Service.post(
            'order/' + this.table_id,
            { username: this.username },
            function (s, res) {
                this.setState({
                    dataOrder: res.message.result,
                    countOrder: res.message.count,
                })
            }.bind(this)
        )
    }

    redirectMenu = (event) => {
        this.setState({ redirect: true })
    }

    render() {
        {
            if (this.state.redirect) return <Redirect push to="/Menu" />
        }
        {
            return (
                <div className="container-fluid">
                    <input
                        className="btn btn-primary m-4"
                        type="button"
                        value="Retour au menu"
                        onClick={this.redirectMenu}
                    />
                    <div className="container-fluid col-8">
                        {
                            this.state.dataOrder.map((order) => {
                                return (
                                    <div className="container-fluid m-5">
                                        <h1 className="display-5 m-4">Order</h1>
                                        <Table responsive>
                                            <thead>
                                                <tr>
                                                    <th>Nom du produit</th>
                                                    <th>Quantite</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {
                                                    order.products.map((product) => {
                                                        return (
                                                            <tr>
                                                                <td> {product.product.title} </td>
                                                                <td> {product.quantity} </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody></Table>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            )
        }
    }
}