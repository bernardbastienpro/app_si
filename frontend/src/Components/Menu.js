import React, {Component} from 'react';
import CommandList from './CommandList';
import MenuItem from './MenuItem';
import Service from "./API";
import Carousel from "react-bootstrap/Carousel";
import Cookies from "universal-cookie";
import {Redirect} from 'react-router-dom'


export default class Carte extends Component{

    constructor(props) {
      super(props);
      this.cookies = new Cookies()
      this.restaurant_id= this.cookies.get('restaurant')
      };

    state = {
      redirect:false,

      countCategory: 0,
      dataCategory: [],
      errorCategory: false,

      countProduct: [],
      dataProducts: [],
      errorProduct: false
    }


    componentDidMount() {
      this.initDataApi()
    }

    initDataApi(){
      this.getCategories()
      this.getProducts()
      for(let product of Object.values(localStorage)){
        console.log(product)
      }
    }

    getCategories(){
      Service.get("product/category/"+this.restaurant_id, function(s,res)
      {
        console.log(res)
        this.setState({ dataCategory:res.message.result, countCategory:res.message.count })
      }
          .bind(this))
    }

    getProducts(){
      Service.get("product/"+this.restaurant_id, function(s,res)
      {

        res.message.result.map(product=>{
          product.quantity=0;
        })

        this.setState({ dataProducts:res.message.result, countProduct:res.message.count })
        console.log(this.state.dataProducts)

      }
          .bind(this))
    }

    getProductByCat(category){
      return(<CommandList products={this.state.dataProducts.filter(product => product.category===category)}/>)
    }

    static sendOrder(){
      let cookies = new Cookies()

      let products = Object.values(localStorage).map(product => {
         product = JSON.parse(product); let resp = {}
        resp.quantity = product.quantity
        resp.product = product._id
         return resp})


      Service.put(
          'order/' + cookies.get('table_id'),
          { username: cookies.get('username'),products:products },
          function (s, res) {console.log(res)
          if(res.error== false){
            alert("Votre commande a bien été envoyé en cuisine !")
            return <Redirect push to="/Recap"/>
          }
          else{
            alert("Une erreur est survenue dans votre commande, réessayé plus tard")
          }
          })
    }

    render() {
      return (
      <Carousel interval={null}>
        {
          this.state.dataCategory.map(category =>{
          return<Carousel.Item key={"car_"+category.name}>
            <Carousel.Caption>
              <MenuItem key={"title_"+category.name} title={category.name}/>
              {this.getProductByCat(category.name)}
            </Carousel.Caption>
            <img
                className="d-block w-100"
                src={"https://via.placeholder.com/"+window.innerWidth+"x"+window.innerHeight+"/14B80E/80808/?text=."}
                alt="First slide"
            />
          </Carousel.Item>
          })
        }
          </Carousel>
      );
    }
    }

