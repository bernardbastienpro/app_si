import React, {Component} from 'react';
import CardItem from "./CardItem";
import {CardColumns} from 'reactstrap';


export default class CommandList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <CardColumns>
                    {this.props.products.map(product => {
                        return (
                            <div>
                                {/* <CardItem imageurl={"https://static.lexpress.fr/medias_12020/w_2048,h_1146,c_crop,x_0,y_154/w_480,h_270,c_fill,g_north/v1550742170/sushi-saumon-maki-saumon-japonais_6154396.jpg"} title={"Title"} max="7" description={"test description"}/> */}
                                <CardItem product={product} max={5}/>
                            </div>)
                    })}

                </CardColumns>
            </div>
        );
    }
}


/*function commandList(){
  return();
}*/
