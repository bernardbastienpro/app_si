import React, {Component} from 'react';
import Card from "react-bootstrap/Card";
import CounterButton from "./CounterButton";
import Popover from "react-bootstrap/Popover";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

export default class CardItem extends Component {

    static defaultProps = {
        title: "Titre",
        max: 5,
        description: undefined,
        image_path: "https://img.icons8.com/carbon-copy/100/000000/no-image.png"
    }

    constructor(props) {
        super(props);
        this.id = props.product._id
        this.title = props.product.title;
        this.max = props.max;
        this.image_path = "http://185.216.27.63:3001/" + props.product.image_path;
        this.description = props.product.description;
    }

    render() {

        const popover = (
            <Popover id="popover-basic">
                <Popover.Title as="h3">Description</Popover.Title>
                <Popover.Content>
                    {this.description}
                </Popover.Content>
            </Popover>
        );

        const Description = () => (
            <OverlayTrigger trigger="click" rootClose placement="right" overlay={popover}>
                <IconButton><InfoIcon fontSize="small"/></IconButton>
            </OverlayTrigger>
        );

        return (
            <Card key={"card_" + this.title}>
                <Card.Img src={this.image_path}/>
                <Card.Body>
                    <Card.Title>
                        {this.title}
                        {this.description !== undefined ? <Description/> : ''}
                    </Card.Title>
                    <CounterButton product={this.props.product} max={this.max}/>
                </Card.Body>
            </Card>
        );
    }
}
