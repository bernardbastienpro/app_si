# MyLunch Front-End
This project is the front part of MyLunch, it's not finished yet but the main features are implemented.
So you can with a special URL or QR code access to a table and choose your plates.

## Launching project
- **npm install**
to get all dependancies of the project

- **npm start**
to launch the front-end project on port :3000

- **npm run build**
to run the front-end project in production

## Main routes 

Base URL = http://localhost:3000/

|                |Parameter					     |Feature|
|----------------|-------------------------------|-----------------------------|
|/Menu		     |none            				 |Display the menu of the restaurant           |
|/Reservation    | ?idTable=`id of the table`    |Reserve a table if it's available            |
|/Recap 		 |none						     |Gives recap of last order|
|/Order 	     |?restaurant_id =`id of the restaurant` | View all the orders of a restaurant



Example url : 
- http://localhost:3000/Reservation?idTable=5f05ff495644191a200bc5ac
- http://localhost:3000/Order?restaurant_id=5efae8b1c6a54e09f8683504


## Navigation for the customer

1. /Reservation?idTable=12345  --> Put username (if cookies are not present) --> /Menu (Table joined)

2. /Menu <--> /Recap 

