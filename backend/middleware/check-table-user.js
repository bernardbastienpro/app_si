const { body, validationResult, check } = require('express-validator');
const apiResponse = require('../utils/api_response');
const TableController = require('../controllers/table');


module.exports = [
    body('username').isLength({ min: 2 }).trim().withMessage('TABLE_USERNAME_MIN_2_CHAR'),
    check('username').trim().escape().isLowercase(),
    check('table_id').isMongoId(),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, { "code": "BODY_INPUTS_INVALID", "details": errors.errors })
        }
        const tableId = req.params.table_id;
        TableController.table_username_exists(tableId, req.body.username)
            .then((usernameExists) => {
                if (!usernameExists)
                    return apiResponse.ErrorResponse(res, "TABLE_OR_USERNAME_DOES_NOT_EXIST")
                next()

            }).catch((e) => {
                return apiResponse.ErrorResponse(res, { "code": "UNKNOWN_ERROR", "details": e.toString() })
            })
    }
]