const apiResponse = require('../utils/api_response');
const Table = require('../models/table')
const Product = require('../models/product')

function getProductRestaurantId(productId) {
    return Product.findOne({ _id: productId })
        .select("restaurant")
        .exec()
        .then(result => {
            return result.restaurant;
        }).catch(() => {
            return null
        })
}

function getTableRestaurantId(tableId) {
    return Table.findOne({ _id: tableId })
        .select("restaurant")
        .exec()
        .then((result) => {
            return result.restaurant;
        }).catch(() => {
            return null
        })
}

module.exports = [
    async(req, res, next) => {
        const productsReq = req.body.products;
        if (!productsReq || productsReq.lenght === 0)
            return apiResponse.ErrorResponse(res, "NO_PRODUCTS_OR_MENUS_FOUND")

        try {
            const restaurantId = await getTableRestaurantId(req.params.table_id)
            if (!restaurantId)
                throw new Error("RESTAURANT_NOT_FOUND")
            await Promise.all(productsReq.map(
                async(product) => {
                    if (!product.product)
                        throw new Error("PRODUCT_ID_MISSING")
                    const productRestaurant = await getProductRestaurantId(product.product)
                    if (!productRestaurant)
                        throw new Error("PRODUCT_NOT_FOUND")
                    if (productRestaurant.toString() !== restaurantId.toString())
                        throw new Error("PRODUCT_NOT_IN_RESTAURANT")
                }
            ))
            next()
        } catch (err) {
            return apiResponse.ErrorResponse(res, err.message)
        }
    }
]