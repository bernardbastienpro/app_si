const apiResponse = require('../utils/api_response');
const TableController = require('../controllers/table');


module.exports = [
    (req, res, next) => {
        const tableId = req.params.table_id;
        TableController.customer_info(tableId, req.body.username)
            .then((userInfos) => {
                if (!userInfos.master) {
                    return apiResponse.ErrorResponse(res, 'TABLE_USERNAME_NOT_MASTER');
                }
                next()

            }).catch((e) => {
                return apiResponse.ErrorResponse(res, { "code": "UNKNOWN_ERROR", "details": e.toString() })
            })
    }
]