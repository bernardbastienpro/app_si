const { validationResult, check } = require('express-validator');
const apiResponse = require('../utils/api_response');
const OrderController = require('../controllers/order');

module.exports = [
    check('order_id').isMongoId(),
    async(req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, { "code": "BODY_INPUTS_INVALID", "details": errors.errors })
        }
        const orderDetails = await OrderController.order_details(req.body.order_id);
        if (!orderDetails) {
            return apiResponse.ErrorResponse(res, "ORDER_NOT_FOUND");
        }

        if (orderDetails.table.toString() === req.params.table_id) {
            next();
        } else {
            return apiResponse.ErrorResponse(res, 'ACTIONS_NOT_ALLOWED_ON_THIS_ORDER');
        }
    }
]