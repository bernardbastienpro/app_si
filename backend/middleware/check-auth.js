/* eslint-disable consistent-return */
/* eslint-disable func-names */
const jwt = require('jsonwebtoken')
const apiResponse = require('../utils/api_response');

module.exports = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['access-token'];
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
            if (err) {
                return apiResponse.ErrorResponse(res, 'Unauthorized access');
            }
            req.decoded = decoded;
            next();
        });
    } else {
        return apiResponse.ErrorResponse(res, 'No token provided');
    }
}