const express = require("express");

const router = express.Router();

const OrderController = require('../controllers/order');

router.post("/:table_id", OrderController.order_get_all_by_username);
router.post("/master/:table_id", OrderController.order_get_all_by_table);
router.put("/:table_id", OrderController.order_create_order);
router.put("/decline/:table_id", OrderController.order_decline_order);
router.put("/cancel/:table_id", OrderController.order_cancel_order);

module.exports = router;