const express = require("express");

const router = express.Router();

const UserController = require('../controllers/user');

router.get("/", UserController.user_get_all);
router.get("/signup/:name", UserController.user_signup);
module.exports = router;