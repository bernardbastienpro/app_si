const express = require("express");

const router = express.Router();

const RestaurantController = require('../controllers/restaurant');

/**
 * @swagger
 * definitions:
 *   Restaurant:
 *     properties:
 *       _id: 
 *          type: string
 *          format: uuid
 *       name:
 *         type: string
 *       localisation:
 *         type: string
 *       type:
 *         type: boolean
 */

/**
 * @swagger
 * /restaurant:
 *   get:
 *     tags:
 *       - Restaurant
 *     description: Returns all restaurants
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of restaurant
 *         schema:
 *           type: array
 *           items:
 *              $ref: '#/definitions/Restaurant'
 */
router.get("/", RestaurantController.restaurant_get_all);
module.exports = router;