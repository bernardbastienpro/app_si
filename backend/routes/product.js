const express = require("express");

const router = express.Router();
const productController = require('../controllers/product');

/**
 * @swagger
 * definitions:
 *   ProductCategory:
 *     properties:
 *       _id:
 *         type: string
 *         format: uuid
 *       name:
 *         type: string
 *       restaurant:
 *         type: string
 *         format: uuid
 * 
 *   Product:
 *     properties:
 *       _id: 
 *          type: string
 *          format: uuid
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       restaurant:
 *         type: string
 *         format: uuid
 *       category:
 *         type: string
 *       image_path:
 *         type: string
 *       price:
 *         type: number
 *         format: float
 *       quantity:
 *         type: number
 *         format: int64
 *       createdAt:
 *         type: string
 *         format: date-time
 *       updatedAt:
 *         type: string
 *         format: date-time 
 */

/**
 * @swagger
 * /product/{restaurant_id} :
 *   get:
 *     tags:
 *       - Product
 *     description: Returns all products for a specific restaurant
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: restaurant_id
 *         description: the MongoDb restaraurant id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: An array of products available for a specific restaurant
 *         schema:
 *           type: array
 *           items:
 *              $ref: '#/definitions/Product'
 * 
 * /product/category/{restaurant_id} : 
 *   get:
 *     tags:
 *       - Product
 *     description: Returns all products categories for a specific restaurant
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: restaurant_id
 *         description: the MongoDb restaraurant id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: An array of categories products available for a specific restaurant
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/ProductCategory'
 * 
 */
router.get("/:restaurant", productController.product_get_all);
router.get("/category/:restaurant", productController.category_get_all);
router.post("/upload", productController.upload)
module.exports = router;