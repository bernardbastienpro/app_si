const express = require("express");

const router = express.Router();

const OrderController = require('../../controllers/order');

router.get("/:table_id", OrderController.order_get_all_by_restaurant);
module.exports = router;