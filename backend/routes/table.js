const express = require("express");

const router = express.Router();

const TableController = require('../controllers/table');

/**
 * @swagger
 * definitions:
 *   Customer:
 *     properties:
 *       _id:
 *         type: string
 *         format: uuid
 *       username:
 *         type: string
 *         format: uuid
 *       master:
 *         type: boolean
 *   Table:
 *     properties:
 *       _id:
 *         type: string
 *         format: uuid
 *       min:
 *         type: number
 *         format: int64
 *       max:
 *         type: number
 *         format: int64
 *       current:
 *         type: number
 *         format: int64
 *       available:
 *         type: boolean
 *       restaurant:
 *         type: string
 *         format: uuid
 *       customers:
 *         type: array
 *         items:
 *           $ref: '#/definitions/Customer'
 *       createdAt:
 *         type: string
 *         format: date-time
 *       updatedAt:
 *         type: string
 *         format: date-time     
 */

/**
 * @swagger
 * /table/join/{table_id} :
 *   post:
 *     tags:
 *       - Table
 *     description: Join a table
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: table_id
 *         description: the MongoDb table id
 *         in: path
 *         required: true
 *         type: string
 *       - name: username
 *         in: body
 *         required: true
 *         type: string
 *         description: the customer username
 *     responses:
 *       200:
 *         description: Some of table and customer informations
 *         schema:
 *           type: object
 *           properties:
 *             joined:
 *               type: boolean
 *             customer:
 *               $ref: '#/definitions/Customer'
 *             table_id:
 *               type: string
 *               format: uuid
 *               description: the MongoDb table id
 * 
 * /table:
 *   get:
 *     tags:
 *       - Table
 *     description: return all tables id 
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: all tables for all restaurants
 *         schema:
 *           type: object
 *           properties:
 *             _id:
 *               type: string
 *               format: uuid
 *             restaurant:
 *               $ref: '#/definitions/Restaurant'
 * 
 * /table/customer/info/{table_id}:
 *   post:
 *     tags:
 *       - Table
 *     description: Get customer informations (master or not)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: table_id
 *         description: the MongoDb table id
 *         in: path
 *         required: true
 *         type: string
 *       - name: username
 *         description: customer username
 *         in: body
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The customer informations
 *         schema:
 *           type: object
 *           properties:
 *             master:
 *               type: boolean
 *             username:
 *               type: string
 * 
 * /table/disponibility/{table_id}:
 *   get:
 *     tags:
 *       - Table
 *     description: Returns disponibility for a specific table
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: table_id
 *         description: the MongoDb table id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The disponibility of a table
 *         schema:
 *           type: object
 *           properties:
 *             _id:
 *               type: string
 *               format: uuid
 *             available:
 *               type: boolean 
 *             current:
 *               type: number
 *               format: int64
 *             max:
 *               type: number
 *               format: int64
 */

router.get("/disponibility/:table_id", TableController.table_disponibility)
router.post("/join/:table_id", TableController.table_join);
router.get("/create/:id", TableController.table_create_table);
router.get("/", TableController.table_get_all);
router.get("/:restaurant_id", TableController.table_get_by_restaurant);
router.post('/customer/info/:table_id', TableController.table_customer_info);
module.exports = router;