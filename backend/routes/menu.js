const express = require("express");

const router = express.Router();

const menuController = require('../controllers/menu');

router.get("/", menuController.create_menu);
router.get("/category", menuController.create_menu_category);
router.get("/all", menuController.get_all_menu);


module.exports = router;