const mongoose = require("mongoose");
const { validationResult, check } = require('express-validator');
const Order = require('../models/order');
const TableController = require("./table")
const apiResponse = require('../utils/api_response');
const checkTableUser = require('../middleware/check-table-user')
const orderValidator = require('../middleware/order-validator');
const checkUserMaster = require("../middleware/check-user-master");
const checkOrderMasterActions = require("../middleware/order-master_actions");

exports.order_details = async(orderId) => {
    try {
        const order = await Order.findOne({ _id: orderId })
            .select();
        return order;
    } catch (e) {
        return null;
    }
}

exports.order_get_all_by_restaurant = [
    (req, res, _next) => {
        Order.find({ restaurant: req.params.restaurant_id })
            .select()
            .populate({
                path: 'products.product',
                populate: {
                    path: 'category',
                    select: 'name'
                }
            })
            .exec()
            .then((orders) => {
                apiResponse.SuccessResponse(res, orders)
            })
            .catch((err) => {
                apiResponse.ErrorResponse(res, err)
            })
    }
]

exports.order_get_all_by_username = [
    checkTableUser,
    (req, res, _next) => {
        Order.find({ table: req.params.table_id, username: req.body.username })
            .select()
            .populate({
                path: 'products.product',
                populate: {
                    path: 'category',
                    select: 'name'
                }
            })
            .sort({ 'createdAt': -1 })
            .exec()
            .then((orders) => {
                return apiResponse.SuccessResponse(res, orders)
            })
            .catch((e) => {
                return apiResponse.ErrorResponse(res, e.message)
            })
    }
]

exports.order_master_accepted_orders = [
    checkTableUser,
    checkUserMaster,
    checkOrderMasterActions,

    (req, res, _next) => {
        Order.updateOne({ _id: req.params.order_id }, { $set: { status: 'sent' } })
            .then((u) => {
                console.log(u);
                return apiResponse.SuccessResponse(res, 'declined : ok');
            })
            .catch((e) => {
                console.log(e);
                return apiResponse.ErrorResponse(res, 'declined : failed');
            })
    }
]

exports.order_cancel_order = [
    checkTableUser,
    check('order_id').isMongoId(),
    async(req, res, _next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, { "code": "BODY_INPUTS_INVALID", "details": errors.errors })
        }
        const orderDetail = await this.order_details(req.body.order_id);
        if (!orderDetail) {
            return apiResponse.ErrorResponse(res, 'ORDER_NOT_FOUND');
        }
        if (orderDetail.username !== req.body.username) {
            return apiResponse.ErrorResponse(res, "ORDER_CANCEL_NOT_ALLOWED");
        }
        const orderDelete = (await Order.deleteOne({ _id: req.body.order_id })).deletedCount;
        if (orderDelete === 1) {
            return apiResponse.SuccessResponse(res, 'ORDER_CANCEL_SUCCESS');
        }
        return apiResponse.ErrorResponse(res, 'ORDER_CANCEL_ERROR');
    }
]

exports.order_decline_order = [
    checkTableUser,
    checkUserMaster,
    checkOrderMasterActions,

    (req, res, _next) => {
        Order.updateOne({ _id: req.body.order_id }, { $set: { status: 'declined' } })
            .then(() => {
                return apiResponse.SuccessResponse(res, 'Order decline status : success');
            })
            .catch(() => {
                return apiResponse.ErrorResponse(res, 'Order decline status : failed');
            })
    }
]

exports.order_get_all_by_table = [
    checkTableUser,
    checkUserMaster,
    (req, res, _next) => {
        Order.find({ table: req.params.table_id, status: 'waiting' })
            .select()
            .populate({
                path: 'products.product',
                populate: {
                    path: 'category',
                    select: 'name'
                }
            })
            .sort({ 'createdAt': -1 })
            .exec()
            .then((orders) => {
                return apiResponse.SuccessResponse(res, orders)
            })
            .catch((e) => {
                return apiResponse.ErrorResponse(res, e.message)
            })
    }
]

exports.order_create_order = [
    checkTableUser,
    orderValidator,
    async(req, res, _next) => {
        const restaurantId = await TableController.tableGetRestaurant(req.params.table_id);
        const order = Order({
            _id: mongoose.Types.ObjectId(),
            restaurant: restaurantId,
            table: req.params.table_id,
            username: req.body.username,
            products: req.body.products,
        })
        order.save()
            .then((orderResponse) => {
                return apiResponse.SuccessResponse(res, orderResponse)
            })
            .catch(e => {
                return apiResponse.ErrorResponse(res, { "code": "CREATE_ORDER_FAILED", "details": e.message })
            });
    }
]