/* eslint-disable no-console */
const mongoose = require('mongoose');
const Menu = require('../models/menu');
const MenuCategory = require('../models/menuCategory');
const apiResponse = require('../utils/api_response');

exports.create_menu = (req, res, next) => {
    const menu = Menu({
        _id: new mongoose.Types.ObjectId(),
        restaurant: "5efae8b1c6a54e09f8683504",
        title: "menu sushi",
        price: 15,
        menuCategories: [
            "5f02f1a38ef5ac3915fb7fd6"
        ]
    })
    menu.save().catch(e => {
        console.log(e);
    });
    return apiResponse.SuccessResponse(res, "menu créé")
}

exports.create_menu_category = (req, res, next) => {
    const menu = MenuCategory({
        _id: new mongoose.Types.ObjectId(),
        restaurant: "5efae8b1c6a54e09f8683504",
        title: "crêpes",
        products: [
            "5efe4f8d3618b91fb8896c11",
            "5efe4fec56649f27e416d3c4",
        ]
    })
    menu.save().catch(e => {
        console.log(e);
    });
    return apiResponse.SuccessResponse(res, "menu categorie créé")
}

exports.get_all_menu = (req, res, next) => {
    Menu.find()
        .select()
        .populate('menuCategories')
        .populate('menuCategories.products')
        .exec()
        .then(docs => {
            return apiResponse.SuccessResponse(res, docs);
        }).catch((e) => {
            return apiResponse.ErrorResponse(res, e);
        })

}