const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
const User = require('../models/user');

exports.get_token = (req, res, next) => {
    User.findOne({ name: req.params.name })
        .select()
        .exec()
        .then(doc => {
            if (doc == null)
                return res.json({ "error": true, "message": "user not found" })
            console.log(doc)
            const token = jwt.sign({ doc },
                process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRATION
                }
            );
            // return the JWT token for the future API calls
            res.json({
                success: true,
                message: 'Authentication successful!',
                token
            });
        })
        .catch((e) => {
            console.log(e)
            res.json({ "error": false })
        })
}

exports.verify_token = (req, res, next) => {
    res.json({
        success: true,
        message: 'Token valid',
    });

}

exports.user_signup = (req, res, next) => {
    const user = User({
        _id: mongoose.Types.ObjectId(),
        name: req.params.name,
        // email: "test@gmail.com"
    });
    user.save().then(result => {
        return res.status(200).json({ "error": false, "message": result });
    }).catch(error => {
        console.log(error)
        return res.status(400).json({ "error": true, "message": "User could'nt create. Maybe email already defined" });
    });
}

exports.user_get_all = (req, res, next) => {
    User.find()
        .select("name _id")
        .exec()
        .then(docs => {
            const response = docs.map(doc => {
                return {
                    name: doc.name,
                    _id: doc._id
                };
            });
            return res.status(200).json({ "error": false, "message": response });
        })
}