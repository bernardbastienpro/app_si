const mongoose = require("mongoose");
const { validationResult, check } = require('express-validator');
const Table = require('../models/table');
const apiResponse = require('../utils/api_response');
const checkDuplicateUsername = require('../middleware/check-duplicate-username');
const checkTableUsername = require('../middleware/check-table-user');

async function tableGetRestaurant(tableId) {
    return Table.findOne({ _id: tableId })
        .select("restaurant")
        .exec()
        .then(restaurant => {
            if (restaurant.restaurant)
                return restaurant.restaurant
            throw new Error("RESTAURANT_NOT_FOUND")
        })
        .catch(e => {
            throw new Error(e)
        })
}

async function joinTable(res, table, username) {
    if (table.current >= table.max)
        return Promise.resolve(apiResponse.ErrorResponse(res, "TABLE_MAX_CAPACITY"))
    if (!table.available)
        return Promise.resolve(apiResponse.ErrorResponse(res, "TABLE_CLOSED"))

    const customer = {
        username,
    }
    if (table.current === 0) {
        customer.master = true
    }

    return Table.updateOne({ _id: table._id }, { $addToSet: { customers: customer }, $set: { current: table.current + 1 } })
        .then(() => {
            tableGetRestaurant(table._id)
                .then(restaurant => {
                    return apiResponse.SuccessResponse(res, { "joined": true, "restaurant": restaurant, "customer": customer, "tableId": table._id })
                })
                .catch(e => {
                    throw new Error(e)
                })
        })
        .catch((e) => {
            return apiResponse.ErrorResponse(res, e.message)
        })
}


exports.table_get_all = (req, res, next) => {
    Table.find()
        .select("_id restaurant")
        .populate("restaurant")
        .exec()
        .then(tables => {
            if (tables.length === 0)
                throw new Error("TABLE_NOT_FOUND")
            return apiResponse.SuccessResponse(res, tables)
        })
        .catch(e => {
            return apiResponse.ErrorResponse(res, e.message)
        })
}


exports.table_create_table = (req, res, next) => {
    const table = Table({
        _id: new mongoose.Types.ObjectId(),
        number: req.params.id,
        min: 1,
        max: 4,
        restaurant: "5efae8e467d1c25f98036c27"
    })
    table
        .save()
        .then(() => {
            return apiResponse.SuccessResponse(res, "succes")
        })
        .catch(() => {
            return apiResponse.ErrorResponse(res, "fail")
        });
}

exports.table_disponibility = [
    check('table_id').isMongoId(),
    (req, res, _next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, errors.errors[0].msg)
        }
        Table.findOne({ _id: req.params.table_id })
            .select("_id available current max")
            .then((table) => {
                if (!table)
                    throw new Error("TABLE_NOT_FOUND")
                if (table.current === table.max)
                    throw new Error("TABLE_MAX_CAPACITY")
                return apiResponse.SuccessResponse(res, table)
            })
            .catch(e => {
                return apiResponse.ErrorResponse(res, e.message)
            })
    }
]


exports.table_customer_info = [
    checkTableUsername,
    (req, res, _next) => {
        this.customer_info(req.params.table_id, req.body.username)
            .then((doc) => {
                return apiResponse.SuccessResponse(res, doc);
            })
            .catch((e) => {
                return apiResponse.ErrorResponse(res, e.message);
            })
    }
]

exports.customer_info = (tableId, username) => {
    return Table.findOne({ _id: tableId, "customers.username": username }, )
        .select('customers')
        .exec()
        .then(doc => {
            if (doc) {
                const userInfo = doc.customers.find(customer => customer.username === username);
                return userInfo;
            }

            throw new Error('NOT FOUND');
        })
        .catch((e) => {
            throw new Error(e.message)
        })
}

exports.table_username_exists = async(tableId, username) => {
    try {
        const doc = await Table.findOne({ _id: tableId, "customers.username": username })
            .select();
        if (doc)
            return true;
        return false;
    } catch (e) {
        return true;
    }
}

exports.table_join = [
    checkDuplicateUsername,
    (req, res, _next) => {
        Table.findOne({ _id: req.params.table_id })
            .select("_id current min max available")
            .exec()
            .then(table => {
                if (!table)
                    throw new Error("TABLE_NOT_FOUND");
                joinTable(res, table, req.body.username.toLowerCase())
                    .then(response => {
                        return response
                    })
                    .catch(e => {
                        return apiResponse.ErrorResponse(res, e.message)
                    })
            })
            .catch((e) => {
                return apiResponse.ErrorResponse(res, e.message);
            })
    }
]

exports.table_get_by_restaurant = [
    check("restaurant_id").isMongoId(),
    (req, res, _next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, errors.errors[0].msg)
        }
        Table.find({ restaurant: req.params.restaurant_id })
            .select("_id number")
            .exec()
            .then(tables => {
                if (tables.length === 0)
                    throw new Error("TABLES_NOT_FOUND")
                apiResponse.SuccessResponse(res, tables)
            })
            .catch(e => {
                return apiResponse.ErrorResponse(res, e.message)
            })
    }
]

exports.tableGetRestaurant = tableGetRestaurant