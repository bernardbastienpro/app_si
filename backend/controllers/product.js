const { validationResult, check } = require('express-validator');
const multer = require("multer");
const path = require("path");
const apiResponse = require('../utils/api_response');
const Category = require('../models/product_category');
const Product = require('../models/product');

const storage = multer.diskStorage({
    destination(req, _file, cb) {
        cb(null, 'uploads/product_image')
    },
    filename(req, file, cb) {
        cb(null, req.body.product_id + path.extname(file.originalname))
    }
})
const upload = multer({
    storage,
    limits: { fileSize: 1000000 },
    fileFilter(req, file, cb) {
        if (!file.mimetype.startsWith('image')) {
            return cb(null, false, new Error('IMAGE_EXT_NOT_AUTHORIZED'));
        }
        cb(null, true);
    },
})

exports.upload = [
    upload.single("product_file"),
    (req, res, _next) => {
        if (!req.body.product_id)
            return apiResponse.ErrorResponse(res, "product id not found")
        const { file } = req
        if (file) {
            Product.findOneAndUpdate({ _id: req.body.product_id }, { $set: { image_path: file.path } })
                .then((response) => {
                    if (!response)
                        throw new Error("PRODUCT_NOT_FOUND")
                    return apiResponse.SuccessResponse(res, "FILE_UPLOAD")
                })
                .catch(e => {
                    return apiResponse.ErrorResponse(res, { "code": "FILE_UPLOAD_FAILED", "details": e.message })
                })
        } else {
            return apiResponse.ErrorResponse(res, "NO_IMAGE_FOUND")
        }
    }
]

exports.product_get_all = [
    check('restaurant').isMongoId(),
    (req, res, _next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, { "code": "BODY_INPUTS_INVALID", "details": errors.errors })
        }
        /* const product = Product({
            _id: new mongoose.Types.ObjectId(),
            title: "brochette poulet",
            category: "5f05f8aac34de13dc22d2e33",
            restaurant: "5efae8e467d1c25f98036c27",
            qty_min: 2
        })
        product.save() */
        Product.find({ restaurant: req.params.restaurant })
            .select()
            .populate("category")
            .exec()
            .then(products => {
                const response =
                    products.map(product => {
                        return {
                            _id: product._id,
                            title: product.title,
                            description: product.description,
                            category: product.category.name,
                            price: product.price,
                            quantity: product.quantity,
                            image_path: product.image_path
                        };
                    });
                return apiResponse.SuccessResponse(res, response);
            }).catch(() => {
                return apiResponse.ErrorResponse(res, "RESTAURANT_NOT_FOUND")
            })
    }
]

exports.category_get_all = [
    check('restaurant').isMongoId(),
    (req, res, _next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.ErrorResponse(res, errors.errors[0].msg)
        }
        Category.find({ restaurant: req.params.restaurant })
            .select("_id name")
            .exec()
            .then(categories => {
                if (!categories)
                    throw new Error("PRODUCT_CATEGORIES_NOT_FOUND")
                const response = categories;
                return apiResponse.SuccessResponse(res, response);
            }).catch((e) => {
                return apiResponse.ErrorResponse(res, e.message);
            })
    }
]