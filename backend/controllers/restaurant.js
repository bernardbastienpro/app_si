const Restaurant = require('../models/restaurant');
const apiResponse = require('../utils/api_response');

exports.restaurant_get_all = async(req, res, next) => {
    const restaurants = await Restaurant.find()
        .select("_id name localisation type")
        .exec()
    const response = restaurants.map(doc => {
        return {
            _id: doc.id,
            name: doc.name,
            localisation: doc.localisation,
            type: doc.type
        };
    });
    return apiResponse.SuccessResponse(res, response);
}