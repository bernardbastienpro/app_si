# API SETUP

## Set up the project :

- Create a file named ".env" with :

```
ENV= [dev | prod]
SET_HTTPS=true
HTTP_PORT=3001
HTTPS_PORT=3002
MONGODB_URI=<your mongo db uri>

JWT_SECRET=<your secret>
JWT_EXPIRATION=<token expiration time>

```

- If you want to run https server :
    * Create ssl directory in the project root : 
    ```
     $ mkdir ssl && cd ssl
    ```
    * Generate ssl certificate : 
    ```
     $ openssl genrsa -out server.key
     $ openssl req -new -key server.key -out server.csr
     $ openssl x509 -req -days 9999 -in server.csr -signkey server.key -out server.crt
     $ rm server.csr
    ```
    * In **.env** file, set :
    ```
    SET_HTTPS=true
    ```
## Run the project :

Open your terminal and execute these following commands :

- ```npm install```

- ```npm start``` in production or ```npm run dev``` in UAT



## API DOCUMENTATION

Please follow this link to read the api documentation : [https://gitlab.com/bernardbastienpro/app_si/-/blob/back-end/backend/API_MANUAL.MD](https://gitlab.com/bernardbastienpro/app_si/-/blob/back-end/backend/API_MANUAL.MD)