/* eslint-disable import/no-extraneous-dependencies */
const express = require('express');
const cors = require('cors');
const path = require('path');
const swaggerJSDoc = require('swagger-jsdoc');

const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helmet = require('helmet');
require("dotenv").config();
const compression = require("compression");


const userRoutes = require('./routes/user');
const restaurantRoutes = require('./routes/restaurant');
const productRoutes = require('./routes/product');
const tableRoutes = require('./routes/table');
const menuRoutes = require('./routes/menu');
const orderRoutes = require('./routes/order');
const adminRoutes = require('./routes/admin');
const apiResponse = require('./utils/api_response');

function initMongoDB() {
    mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true, autoIndex: true })
        .catch(() => {
            console.log("MongoDB connection failed");
            process.exit(1)
        });
    mongoose.Promise = global.Promise;
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
}

function initDevEnv() {
    app.use(morgan("dev"));
}

initMongoDB();

if (process.env.ENV === "dev") {
    initDevEnv();
}

const swaggerDefinition = {
    info: {
        title: 'MyLunch',
        version: '1.0.1',
        description: 'Make an order easily',
    },
    host: process.env.SERVER_ADDRESS,
    basePath: '/',
};


const options = {
    swaggerDefinition,
    apis: ['./routes/*.js', './routes/admin/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);
app.use(cors());
app.use(compression());
app.use(helmet());
app.use('/uploads', express.static('uploads'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});
app.use("/admin", adminRoutes);
app.use("/user", userRoutes);
app.use("/restaurant", restaurantRoutes);
app.use("/product", productRoutes);
app.use("/table", tableRoutes);
app.use("/menu", menuRoutes);
app.use("/order", orderRoutes);

app.use((_req, _res, next) => {
    const error = new Error("ROUTE_NOT_FOUND");
    next(error);
});

app.use((error, _req, res, _next) => {
    apiResponse.NotFoundResponse(res, error.message)
});

module.exports = app;