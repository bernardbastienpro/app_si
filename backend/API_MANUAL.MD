# Usage of Lunch API

- HTTPS server URL : [https://185.216.27.63:3002](http://185.216.27.63:3002)
- HTTP server URL : [http://185.216.27.63:3001](http://185.216.27.63:3001)

## Restaurant

### [GET] **/restaurant** - get all restaurants :

- Responses :
    * [200] SUCCESS :
    
    ```
    {
        "error": false,
        "message": {
            count: 1,
            result: [
                {
                    "_id": "id",
                    "name": "name of restaurant",
                    "localisation": "localisation of restaurant",
                    "type": "asian or french for example"
                }
            ]
        }
    }
    ```
    
    * [400] ERROR :
    
    ```
    {
        "error": true,
        "message": "RESTAURANT NOT FOUND | OTHER_SERVER_ERROR"
    }
    ```

## Product

### [GET] **/product/{restaurant_id}** - get all products for a specific restaurant :

- Parameters :
    * (path) restaurant_id => type : MongodbID(String)
    
- Responses :
    * [200] SUCCESS :
    
    ```
    {
        "error": false,
        "message": {
            count: 1,
            result: [
                {
                    "_id": "id",
                    "title": "title of product",
                    "description": "description of product",
                    "category": "category of product",
                    "price": "price of product",
                    "quantity": "the product quantity",
                    "image_path": "the product image path"
                }
            ]
        }
    }
    ```
    
    * [400] ERROR :
    
    ```
    {
        "error": true,
        "message": "RESTAURANT NOT FOUND | BODY_INPUTS_INVALID | OTHER_SERVER_ERROR"
    }
    ```
    
### [GET] **/product/category/{restaurant_id}** - get all products categories for a specific restaurant :

- Parameters :
    * (path) restaurant_id => type : MongodbID(String)
    
- Responses :
    * [200] SUCCESS :
    
    ```
    {
        "error": false,
        "message": {
            count: 1,
            result: [
                {
                    "_id": "category id",
                    "name" : "name of the category"
                }
            ]
        }
    }
    ```
    
    * [400] ERROR :
    
    ```
    {
        "error": true,
        "message": "RESTAURANT NOT FOUND | OTHER_SERVER_ERROR"
    }
    ```
    
## Table

### [GET] **/table/disponibility/{table_id}** - get disponibility of a specific table :
- Parameters :
    * (path) table_id => type : MongodbID(String)
    
- Responses :
    * [200] SUCCESS :
    
    ```
    {
    "error": false,
    "message":
        {
            "available": true | false,
            "_id": "<id of the table>"
        }
    }
    ```
    
    * [400] ERROR :
    
    ```
    {
        "error": true,
        "message": "TABLE NOT FOUND | OTHER_SERVER_ERROR"
    }
    ```
### [POST] **/table/join/{table_id}** - join a table :
- Parameters :
    * (path) table_id => type : MongodbID(String)
    * (body) username => type : String - The username of the customer 
    
- Responses :
    * [200] SUCCESS :
    
    ```
   {
        "error": false,
        "message":
        {
            "joined": true|false,
            "customer": 
            {
                "username": "the customer username",
                "master": true | false
            },
            "table_id": "id of the table"
        }
    }
    ```
    
    * [400] ERROR :
    
    ```
    {
        "error": true,
        "message": "
                    | TABLE_NOT_FOUND
                    | TABLE_USERNAME_EXISTS
                    | TABLE_MAX_CAPACITY
                    | TABLE_CLOSED
                    | OTHER_SERVER_ERROR
                    "
    }
    ```
    
## Order

### [GET] **/order/{table_id}** - get all orders of a user :
- Parameters :
    * (path) table_id => type : MongodbID(String)
    * (body) username => type : String
    
- Responses :
    * [200] SUCCESS :
    
    ```
    {
        "error": false,
        "message": {
            "count": 1,
            "result": [{
                "_id": "the order id",
                "table": "the table id",
                "username": "the username",
                "products": [{
                    "quantity": "the quantity choosen by user",
                    "product": {
                        "price": "the product price",
                        "quantity": "the product quantity",
                        "_id": "the product id",
                        "title": "the title product",
                        "description": "the product description [optional]",
                        "category": {
                            "_id": "the category id",
                            "name": "the category name"
                        },
                        "restaurant": "the restaurant id",
                        "__v": 0
                    }
                }]
            }]
        }
    }
    ```
    
    * [400] ERROR :
    
    ```
    {
        "error": true,
        "message": 
        {
        "code": "BODY_INPUTS_INVALID",
        "details": 
            [
                {
                    "msg": "TABLE_USERNAME_MIN_2_CHAR | INVALID_VALUE | TABLE_OR_USERNAME_DOES_NOT_EXIST",
                    "param": "username",
                    "location": "body"
                }
            ]
        }
    }
    ```

### [PUT] **/order/{table_id}** - create an order :
- Parameters :
    * (path) table_id => type : MongodbID(String)
    * (body) username => type : String
    * (body) products => type : Array - example : 
```
[
    {
        quantity: Number (optional),
        product: MongodbId
    }
]
```
    
- Responses :
    * [200] SUCCESS :
    
    ```
    {
        "error": false,
        "message":
        {
            "_id": "the order id",
            "table": "the table id",
            "username": "the username",
            "products":
            [
                {
                    "quantity": "the quantity choosen for this product",
                    "product": "the product id"
                },
            ],
            "menus": [],
            "createdAt": "2020-07-09T20:57:54.697Z",
            "updatedAt": "2020-07-09T20:57:54.697Z",
            "__v": 0
        }
    }
    ```
    
 * [400] ERROR :
    
    ```
   {
    "error": true,
    "message": "
        TABLE_USERNAME_MIN_2_CHAR
        | INVALID_VALUE
        | TABLE_OR_USERNAME_DOES_NOT_EXIST
        | NO_PRODUCTS_OR_MENUS_FOUND
        | PRODUCT_NOT_IN_RESTAURANT
        "
    }
    ```