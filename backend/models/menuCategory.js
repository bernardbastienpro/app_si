const mongoose = require('mongoose');

const menuCategorySchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: { type: String, required: true, min: 2, lowercase: true },
    description: { type: String, required: false },
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
    products: [
        { type: mongoose.Schema.Types.ObjectId,ref : 'Product', required: true, unique: true }
    ]
    
}, { timestamps: true });

menuCategorySchema.index({ title: 1, restaurant: 1 }, { unique: true })

module.exports = mongoose.model('MenuCategory', menuCategorySchema);