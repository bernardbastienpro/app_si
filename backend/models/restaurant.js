const mongoose = require('mongoose');

const restaurantSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    localisation: { type: String, required: true },
    type: { type: String, required: true }
});

restaurantSchema.index({ name: 1, localisation: 1 }, { unique: true })

module.exports = mongoose.model('Restaurant', restaurantSchema);