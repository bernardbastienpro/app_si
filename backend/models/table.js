const mongoose = require('mongoose');

const tableSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    number: { type: Number, required: true },
    min: { type: Number, required: true },
    max: { type: Number, required: true },
    current: { type: Number, default: 0, max: this.max },
    available: { type: Boolean, default: true },
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
    customers: [{
        _id: mongoose.Schema.Types.ObjectId,
        username: { type: String, required: true, trim: true, lowercase: true, minLength: 2 },
        master: { type: Boolean, default: false }
    }]
}, { timestamps: true });


tableSchema.index({ restaurant: 1, number: 1 }, { unique: true })
tableSchema
    .virtual("bookId")
    .get(function() {
        return `${this.restaurant  }/${  this.number}`;
    })

module.exports = mongoose.model('Table', tableSchema);