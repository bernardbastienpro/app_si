const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: { type: String, required: true, min: 2, lowercase: true },
    description: { type: String, required: false },
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
    category: { type: mongoose.Schema.Types.ObjectId, ref: 'ProductCategory', required: true },
    image_path: { type: String, required: false },
    price: { type: Number, default: 0 },
    quantity: { type: Number, default: 1 }
}, { timestamps: true });

productSchema.index({ title: 1, restaurant: 1 }, { unique: true })

module.exports = mongoose.model('Product', productSchema);