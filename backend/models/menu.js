const mongoose = require('mongoose');

const menuSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: { type: String, required: true, min: 2, lowercase: true },
    description: { type: String, required: false },
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
    price: { type: Number, default: 0 },
    menuCategories: [
         { type: mongoose.Schema.Types.ObjectId,ref : 'MenuCategory', required: true, unique: true }
    ]

}, { timestamps: true });

menuSchema.index({ title: 1, restaurant: 1 }, { unique: true })

module.exports = mongoose.model('Menu', menuSchema);