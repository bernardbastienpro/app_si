const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    table: { type: mongoose.Schema.Types.ObjectId, ref: 'Table', required: true },
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
    username: { type: String, required: true, trim: true, lowercase: true, minLength: 2 },
    menus: [{
        _id: { type: mongoose.Schema.Types.ObjectId },
        product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true },
        user_quantity: { type: Number, default: 1 }
    }],
    products: [{
        _id: { type: mongoose.Schema.Types.ObjectId },
        product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true },
        user_quantity: { type: Number, default: 1 }
    }],
    status: {
        type: String,
        enum: ['accepted', 'sent', 'waiting', 'declined'],
        default: 'waiting'
    }
}, { timestamps: true });

module.exports = mongoose.model('Order', orderSchema);