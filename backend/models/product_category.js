const mongoose = require('mongoose');

const productCategorySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true, min: 2, lowercase: true },
    restaurant: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
});

productCategorySchema.index({ name: 1, restaurant: 1 }, { unique: true })

module.exports = mongoose.model('ProductCategory', productCategorySchema);