const http = require('http');
const https = require('https');
const fs = require('fs');
const app = require('./app');

if (process.env.ENV === "prod") {

    const httpsPort = process.env.HTTPS_PORT || 3002;
    const key = fs.readFileSync('ssl/server.key');
    const cert = fs.readFileSync('ssl/server.crt');
    const httpsServer = https.createServer({ key, cert }, app);
    httpsServer.listen(httpsPort);
}

const httpPort = process.env.HTTP_PORT || 3001;
const httpServer = http.createServer(app);

httpServer.listen(httpPort);