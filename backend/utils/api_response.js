exports.SuccessResponse = function(res, msg) {
    return res.status(200).json(msg);
};

exports.ErrorResponse = function(res, msg) {
    return res.status(403).json(msg);
}

exports.NotFoundResponse = function(res, msg) {
    return res.status(404).json(msg);
};

exports.ValidationErrorWithData = function(res, msg) {
    return res.status(400).json(msg);
};

exports.UnauthorizedResponse = function(res, msg) {
    return res.status(401).json(msg);
};