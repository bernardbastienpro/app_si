# My Lunch

This project is split in two parts : back-end and front-end.

To run this project, you need to follow these steps :

## Back-end

- Run steps : 

https://gitlab.com/bernardbastienpro/app_si/-/blob/master/backend/README.md

- Api documentation :

https://gitlab.com/bernardbastienpro/app_si/-/blob/master/backend/API_MANUAL.MD

## Front-end

https://gitlab.com/bernardbastienpro/app_si/-/blob/master/frontend/README.md